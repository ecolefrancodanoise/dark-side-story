The Dark Side Story
===================

This is the evolving story line supporting our [Dark Side Challenge](https://hackaday.io/project/164082-the-dark-side-challenge) - an educational concept aimed at teaching children how to balance machine intelligence with human decision points. And programming.

It is a [hard science fiction](https://en.wikipedia.org/wiki/Hard_science_fiction) concept revolving around Aė, a female astronaut and teacher, her pupils and their adventures in space.

![AE](poster-color-5.jpg)]
