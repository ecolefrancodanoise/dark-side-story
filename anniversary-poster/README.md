The Dark Side Story
===================

This is the evolving story line supporting our Dark Side Challenge - an educational concept aimed at teaching children how to balance machine intelligence with human decision points. And programming.

It revolves around Aė, a female astronaut and teacher.

![AE](ae.jpg)

She is featured with two astro-kids and each their pet rover, celebrating the fiftieth anniversary of the first manned lunar landing, illustrated by the Eagle landing module in the background.

Also in the background, a gigantic mech suggests that in space's low or zero gravity, size is not an issue.

On the lunar sky, a half-Earth alludes to the dark side concept and a warped grid with planetary orbits suggests connections to mathematical modeling, computational thinking and differential equations.

![Anniversary](50-years.jpg)

![Anniversary](poster-bw.jpg)

![Anniversary](poster-color-1.jpg)

Now with a logo!

![DSC Logo](logo/logo-small.png)

![Anniversary](poster-color-2.jpg)

![Anniversary](poster-color-3.jpg)

![Anniversary](poster-color-4.jpg)

Final version (Apollo 11 anniversary):

![Anniversary](poster-color-5.jpg)
